import pytest
import os
from day_1_part_1 import find_product_of_two_numbers as day1_part1_function
from day_1_part_2 import main as day1_part2_function

# Day 1 (Part 1) test cases
def test_day1_part1():
    input_data = [1721, 979, 366, 299, 675, 1456]
    target = 2020
    expected_result = 514579
    result = day1_part1_function(input_data, target)
    assert result == expected_result

def test_day1_part1_another_case():
    input_data = [1000, 1020, 30, 40, 50]
    target = 2020
    expected_result = 1020000
    result = day1_part1_function(input_data, target)
    assert result == expected_result

# Day 1 (Part 2) test cases
def test_day1_part2(capsys):
    input_data = "1721\n979\n366\n299\n675\n1456"
    with open('input1.txt', 'w') as f:
        f.write(input_data)

    day1_part2_function()
    captured = capsys.readouterr()
    expected_result = "241861950\n"
    assert captured.out == expected_result
